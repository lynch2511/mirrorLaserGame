#include "sensDeplacement.h"

void sensDeplacement::corrigeDeplacment(int d)
{
    if (d<-1)
    {
        d = -1;
    }
    else if (d>1)
    {
        d = 1;
    }
}
int sensDeplacement::dx() const
{
    return d_dx;
}
int sensDeplacement::dy() const
{
    return d_dy;
}
sensDeplacement::sensDeplacement(int dx , int dy ): d_dx{dx}, d_dy{dy}
{
    corrigeDeplacment(d_dx);
    corrigeDeplacment(d_dy);
    if (d_dx!= 0 && d_dy!= 0)
    {
        d_dy = 0;
    }
}


void sensDeplacement::tourneADroite()
{
    auto olddx {d_dx};
    d_dx = -d_dy;
    d_dy = -olddx;
}
void sensDeplacement::tourneAGauche()
{
    auto olddx {d_dx};
    d_dx = d_dy;
    d_dy = -olddx;
}
