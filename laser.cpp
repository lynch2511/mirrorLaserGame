#include "laser.h"

laser::laser()
{

}

laser::laser(char& caractere) : d_caractereLaser{caractere}
{

}

std::string laser::recupererType() const
{
    std::string type =  typeid(*this).name();
    type = type.substr(1,type.length()-1);
    return type;
}


char laser::recupererCaractere() const
{
    return d_caractereLaser;
}

void laser::afficherObjet() const
{
    cout<<d_caractereLaser;
}

