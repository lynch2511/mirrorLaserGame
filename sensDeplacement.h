
#ifndef SENSDEPLACEMENT_H
#define SENSDEPLACEMENT_H

class sensDeplacement
{

public:
    sensDeplacement(int dx =1, int dy =0);
    int dx() const;
    int dy() const;
    void corrigeDeplacment(int d);
    void tourneADroite();
    void tourneAGauche();
private:
    int d_dx, d_dy;


};

#endif

