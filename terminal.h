#ifndef TERMINAL_H
#define TERMINAL_H
#include"afficheur.h"


class terminal : public afficheur
{
    public:
        terminal();
        virtual void reinitialiser() const override;
        virtual void afficherGrille(terrain& t) const override;
        virtual int afficherMenu(string titre, string instructions, vector<string> propositions) const override;
    private:
};

#endif
