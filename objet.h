#ifndef OBJET_H
#define OBJET_H

#include "sensDeplacement.h"
#include <iostream>

using namespace std;
class objet{
public:
    objet();
    objet(string& nom);
    string recupererNomObjet() const;
    virtual char recupererCaractere() const = 0;
    virtual void afficherObjet() const =0;
    virtual std::string recupererType() const = 0;
    virtual ~objet() = default;
    int setX();
    int setY();
    void getX() const;
    void getY() const;

private:
    string d_nomObjet;
    int d_x,d_y;
};

#endif //OBJET_H
