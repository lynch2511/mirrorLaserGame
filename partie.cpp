#include "partie.h"
#include "parseurTerrain.h"
#include "afficheur.h"
#include <string>
#include "miroir.h"
#include "dirent.h"
#include "jeu.h"

partie::partie()
{

}

partie::partie(unique_ptr<terrain> t, unique_ptr<afficheur> a) : d_terrain{std::move(t)}, d_afficheur{std::move(a)}
{
}

void partie::DebutPartie()
{
    d_afficheur->reinitialiser();
    afficherTerrain();
    int selectionPartie = MenuPartie();
    while(selectionPartie != 0)
    {
        switch(selectionPartie)
        {
          case 1: tirer();
                  break;
          case 2: poserMiroir();
                break;
          case 3: retirerMiroir();
                break;
          case 4: reinitialiserTerrain();
                break;
          case 5: sauvegarderPartie();
        }
        selectionPartie = MenuPartie();
    }
}

void partie::afficherTerrain()
{
    d_afficheur->afficherGrille(*d_terrain);
}

unsigned int partie::MenuPartie()
{

    string titre;
    string instructions;
    titre+="Terrain : "+d_terrain->Nom()+"\n";
    titre+="-------------------------------------------------------------------------\n";
    instructions+="Nombre de miroirs a poser disponibles : "+to_string(d_terrain->nbMiroirsDisponibles())+"\n";
    instructions+="Nombre de miroirs a poser maximum : "+ to_string(d_terrain->nbMiroirsMax())+"\n";
    instructions+="-------------------------------------------------------------------------\n";
    vector<string> propositions{};
    propositions.push_back("Tirer");
    propositions.push_back("Poser un miroir");
    propositions.push_back("Retirer un miroir");
    propositions.push_back("Réinitialiser le terrain");
    propositions.push_back("Sauvegarder");

    return d_afficheur->afficherMenu(titre,instructions,propositions);
}

void partie::poserMiroir()
{
    if(d_terrain->nbMiroirsDisponibles() != 0)
    {
        int x,y;
        cout<<"Saisir les coordonnees : x,y"<<endl;
        cin>>x>>y;

        char charMiroir = ' ';
        cout<<"Saisir l'orientation du miroir :"<<endl;
        cin>>charMiroir;

        d_terrain->insereObjet(make_unique<miroir>(charMiroir,90),x-1,y-1);
        afficherTerrain();
    }
}
void partie::retirerMiroir()
{
    if(d_terrain->nbMiroirsDisponibles() != 5)
    {
        int x,y;
        cout<<"Saisir les coordonnees : x,y"<<endl;
        cin>>x>>y;

        d_terrain->retirerObjet(x-1,y-1);
        afficherTerrain();
    }

}
void partie::tirer()
{

}
void partie::reinitialiserTerrain()
{

}
void partie::abandonnerPartie()
{

}

bool partie::partieExiste(const string& nomFichier)
{
    DIR* rep;
    rep=opendir(repertoireSauvegarde.c_str() );
    if(rep==nullptr)
    {
        cout<<"impossible d'ouvrir le repertoire"<<repertoireSauvegarde<<endl;
    }
    else
    {
        dirent* elem;
        string nom="";
        while( (elem=readdir(rep)) != nullptr)
        {
            nom=elem->d_name;
            if( nom != "." && nom != ".." )
            {
                if(nomFichier==nom)
                {
                    cout<<"un fichier de meme nom existe deja"<<endl;
                    return true;
                }
            }
        }
    }
    closedir(rep);
    return false;
}

void partie::sauvegarderPartie()
{
    cout<<"donner un nom de sauvegarde"<<endl;
    string nom;
    cin>>nom;
    nom+= ".txt";
    if(partieExiste(nom)==false)
    {
        ofstream fichierOut(repertoireSauvegarde+ '\\' + nom);
        if(fichierOut)
        {
            string terrainASauver=parseurTerrain::parse(*d_terrain);
            fichierOut<<terrainASauver;
            cout<<"fichier sauve correctement"<<endl;
        }
    }
}


unique_ptr<terrain> partie::recupererTerrain()
{
    return std::move(d_terrain);
}
