#include "terminal.h"
#include <string>
#include "tool.h"

terminal::terminal()
{

}

void terminal::reinitialiser() const
{
    system("cls");
}

void terminal::afficherGrille(terrain& t) const
{
    for(int i=0;i<=t.nbLignes();i++)
    {
        for(int j=0;j<t.nbColonnes();j++)
        {
            if(i!=t.nbLignes())
            {
                if(j==0)
                    // Ecriture du num�ro de ligne
                    cout << setw(3) << i + 1 << setw(2);
                else
                    cout << setw(3);

                // Affichage objet
                if(t.getObjet(i,j)!=nullptr)
                    t.getObjet(i,j)->afficherObjet();
                else
                    cout<<" ";
            }
            else
            {
                if(j==0)
                    // Ecriture du num�ro de colonne
                    cout << setw(5) << j + 1;
                else
                    cout << setw(3) << j + 1 << setw(1);
            }
        }
        cout<<endl;
    }
}


// A d�velopper pour rendre le menu g�n�rique
int terminal::afficherMenu(string titre, string instructions, vector<string> propositions) const
{
    string choix;
    unsigned int choixInt = -1;
    if(propositions.size()>=0)
    {
        do
        {
            cout<<titre<<endl;
            cout<<instructions<<endl;
            for(int i = 0; i < propositions.size(); i++)
            {
                cout<<to_string(i+1)+") "+propositions[i]<<endl;
            }
            if(propositions.size() == 0) {cout<<"Aucun element a afficher!"<<endl;}
            cout<<"0) Quitter\n"<<endl;
            cin>>choix;
            // Essayer de rendre la saisie en un entier
            choixInt = tool::tryParse(choix);
        }
        while(choixInt < 0 || choixInt > propositions.size());
    }
    return choixInt;
}

