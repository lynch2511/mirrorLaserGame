#include "terrain.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include "objet.h"
#include "laser.h"
#include "parseurTerrain.h"
#include "miroir.h"
#include "cible.h"
#include <algorithm>
using namespace std;


terrain::terrain(const int nbLignes, const int nbColonnes, const int nbMiroirsMax, const mur &m, const string& nom) : d_nbColonnes{ nbColonnes }, d_nbLignes{ nbLignes }, d_nbMiroirsMax{nbMiroirsMax}, d_mur{ m }, d_grille{}, d_nom{nom}
{
    terrain::initialiserTerrainVide(nbLignes,nbColonnes);
}

int terrain::nbColonnes() const
{
    return d_nbColonnes;
}

int terrain::nbLignes() const
{
    return d_nbLignes;
}

int terrain::nbMiroirsMax() const
{
    return d_nbMiroirsMax;
}

mur terrain::getMur() const
{
    return d_mur;
}
/*position terrain::getPosition() const
{
    return d_pos;
}
*/
string terrain::Nom() const
{
    return d_nom;
}

int terrain::nbMiroirsPoses() const
{
    int nb = 0;
    string toString = parseurTerrain::parse(*this);
    nb = count(toString.begin(), toString.end(), MIROIR); // adapter pour les deux types d'affichage de miroir
    return nb;
}

int terrain::nbMiroirsDisponibles() const
{
    return nbMiroirsMax() - nbMiroirsPoses();
}

objet* terrain::getObjet(int x, int y) const
{
     if(!d_grille.empty() && x>=0 && y>=0)
     {
        if(d_grille[x][y]!=nullptr)
        {
            return d_grille[x][y].get();
        }
     }
     return nullptr;
}


void terrain::insereObjet(unique_ptr<objet> obj, const int x, const int y)
{
    if(!d_grille.empty() && x>=0 && y>=0)
    {
        if(getObjet(x,y) == nullptr) {

            if(y < d_grille[0].size() && x < d_grille.size())
            {
                 char test = obj->recupererCaractere();
                 switch (test)
                 {
                     case MUR:
                        d_grille[x][y] = make_unique<mur>(test);
                     case LASER:
                        d_grille[x][y] = make_unique<laser>(test);
                     case CIBLE:
                        d_grille[x][y] = make_unique<cible>(test);
                     case MIROIR:
                        d_grille[x][y] = make_unique<miroir>(test,180);
                 }
            }
        }
        else {
            cout<<"Impossible de poser un objet sur cette case\n"<<endl;
        }

    }
}

void terrain::retirerObjet(const int x,const int y)
{
    if(!d_grille.empty() && x>0 && x < d_grille.size()-1 && y>0 && y < d_grille[0].size()-1)
    {
        if(getObjet(x,y) != nullptr) {
            if(getObjet(x,y)->recupererCaractere() != '#') {
                d_grille[x][y] = nullptr;
            }
            else {
                cout<<"Cette case est un mur, saisir d'autres coordonnees\n"<<endl;
            }
        }
        else {
            cout<<"Cette case est vide, saisir d'autres coordonnees\n"<<endl;
        }
    }
    else
    {
        cout<<"Ceci est une bordure de terrain impossible à retirer\n"<<endl;
    }
}


void terrain::initialiserTerrainVide(const int x, const int y)
{
    for (int i = 0; i<d_nbLignes; i++)
    {
        vector<unique_ptr<objet>> v;
        for(int j = 0; j<d_nbColonnes; j++)
        {
            if(i == 0 || i == d_nbLignes - 1 || j == 0 || j == d_nbColonnes - 1)
            {
                // génère les murs
                v.push_back(move(make_unique<mur>(d_mur.recupererCaractere())));
            }
            else
            {
                v.push_back(nullptr);
            }
        }
        d_grille.push_back(move(v));
    }
}

bool terrain::operator==(const terrain& autreTerrain) const
{
    string toStringThis = parseurTerrain::parse(*this);
    string toStringAutre = parseurTerrain::parse(autreTerrain);
    return toStringAutre == toStringThis;
}

// affiche le terrain (responsabilité transférée à la classe afficheur/terminal)
void terrain::show() const
{
    /*
    for(int i=0;i<=d_nbLignes;i++)
    {
        for(int j=0;j<d_nbColonnes;j++)
        {
            if(i!=d_lignes)
            {
                if(j==0)
                    // Ecriture du numéro de ligne
                    cout << std::setw(3) << i + 1 << std::setw(2);
                else
                    cout << setw(3);

                // Affichage objet
                if(d_grille[i][j]!=nullptr)
                    d_grille[i][j]->afficherObjet();
                else
                    cout<<" ";
            }
            else
            {
                if(j==0)
                    // Ecriture du numéro de colonne
                    cout << std::setw(5) << j + 1;
                else
                    cout << std::setw(3) << j + 1 << std::setw(1);
            }
        }
        cout<<endl;
    }
    */
}


// g�n�re une grille vierge
/*void terrain::genererGrille(const string& ficIn)
{

    ifstream x(ficIn);
    if(x)
    {
        char chaine;
        int l,h,nb;
        x>>l>>h>>nb;
        d_nbMiroirs=nb;
        d_nbColonnes=l;
        d_nbLignes=h;

        for( int i=0 ; i<h; i++)
        {
            vector<unique_ptr<objet>> v;
            for (int j=0 ; j<l; j++)
            {
                x>>chaine;
                if(chaine == 'X')
                {
                    v.push_back(move(make_unique<mur>(chaine)));
                }
                else
                    v.push_back(nullptr);

            }
            d_grille.push_back(move(v));

        }
    }

}
*/
/*
// g�n�re une grille vierge
void terrain::genererGrille()
{
	for (int i = 0; i < d_largeur; i++)
	{
		vector<string> v;
		for (int j = 0; j < d_longueur; j++)
		{
			if (i == 0 || i == d_largeur - 1 || j == 0 || j == d_longueur - 1)
			{
				v.push_back(d_caractere);
			}
			else
			{
				v.push_back(" ");
			}
		}
		d_grille.push_back(v);
	}
}
*/
/*
bool terrain::tirer()
{
    position pos=d_pos;
    sensDeplacement sens(1,0);

    if(pos.x() == d_nbColonnes)
    {
        sens.tourneAGauche();
        sens.tourneAGauche();
    }
    if(pos.y() == 0)
    {
        sens.tourneADroite();
    }
    if(pos.y() == d_nbLignes)
    {
        sens.tourneAGauche();
    }
    pos.avance(sens);
    char c = d_grille[pos.x()][pos.y()]->recupererCaractere();
    while(c!='X' && c!='<' && c!= '-' && c!='|')
    {
        if(c=='/')
        {
            sens.tourneAGauche();
        }
        else if(c=='\\')
        {
            sens.tourneADroite();
        }
        else
        {
             //d_grille[x][y] = make_unique<projectile>(sens);
        }
        pos.avance(sens);
        c = d_grille[pos.x()][pos.y()]->recupererCaractere();

    }
    return c=='<';



}
*/
terrain::~terrain()
{
    //dtor
}
