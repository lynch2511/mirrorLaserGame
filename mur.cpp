#include "mur.h"
mur::mur()
{

}

mur::mur(char caractere) : d_caractereM{caractere}
{

}

std::string mur::recupererType() const
{
    std::string type =  typeid(*this).name();
    type = type.substr(1,type.length()-1);
    return type;
}

char mur::recupererCaractere() const
{
    return d_caractereM;
}

void mur::afficherObjet() const
{
    cout<<d_caractereM;
}

