#ifndef MIROIR_H
#define MIROIR_H

#include "objet.h"

class miroir : public objet{
public:
    miroir();
    miroir(char& caractere, int orientation);
    virtual char recupererCaractere() const override;
    virtual void afficherObjet() const override;
    virtual std::string recupererType() const override;
    virtual ~miroir()=default;
private:
    char d_caractereM;
    int d_orientation;
};

#endif //MIROIR_H
