#include "parseurTerrain.h"
#include "terrain.h"
#include "laser.h"
#include "miroir.h"
#include "cible.h"
#include <vector>
/*
Exemple de formatage :
------------------------------------
contenu de fichier.txt :
[10,10,#,5,{[mur,#,1,4][mur,#,2,4][mur,#,3,4][laser,>,4,0]}]
------------------------------------

Dessin correspondant :
X X X X X X X X X X
X . . . X . . . . X
X . . . X . . . . X
X . . . X . . . . X
> . . . . . . . . X
X . . . . . . . . X
X . . . . . . . . X
X . . . . . . . . X
X . . . . . . . . X
X X X X X X X X X X

*/

string parseurTerrain::parse(const terrain& t)
{
    string str = "[";
    str+=to_string(t.nbLignes())+",";
    str+=to_string(t.nbColonnes())+",";
    str.push_back(t.getMur().recupererCaractere());
    str+=",";
    str+=to_string(t.nbMiroirsMax())+",";
    str+="{";
    for(int i = 0; i<t.nbLignes();i++)
    {
        for(int j=0; j<t.nbColonnes();j++)
        {
            // R�cup�rer les objets qui ne sont pas sur les bords uniquement
            objet* obj = nullptr;
            obj = t.getObjet(i,j);
            if(obj!= nullptr)
            {
                if(obj->recupererCaractere() != t.getMur().recupererCaractere())
                {
                    str+="[";
                    if(obj->recupererCaractere()== MUR)
                    {
                        str+="mur";
                    }
                    if(obj->recupererCaractere()== CIBLE)
                    {
                        str+="cible";
                    }
                    if(obj->recupererCaractere()== LASER)
                    {
                        str+="laser";
                    }
                    if(obj->recupererCaractere()== MIROIR)
                    {
                        str+="miroir";
                    }
                    str+=",";
                    str.push_back((*obj).recupererCaractere());
                    str+=",";
                    str+=to_string(i)+",";
                    str+=to_string(j);
                    str+="]";
                }
                else
                {
                    if(i>0 && j>0 && i<t.nbLignes()-1 && j<t.nbColonnes()-1)
                    {
                        // si c'est un mur, ni j ni i ne doit �tre un bord
                        str+="[";
                        if(obj->recupererCaractere()== MUR)
                        {
                            str+="mur";
                        }
                        if(obj->recupererCaractere()== CIBLE)
                        {
                            str+="cible";
                        }
                        if(obj->recupererCaractere()== LASER)
                        {
                            str+="laser";
                        }
                        if(obj->recupererCaractere()== MIROIR)
                        {
                            str+="miroir";
                        }
                        str+=",";
                        str.push_back((*obj).recupererCaractere());
                        str+=",";
                        str+=to_string(i)+",";
                        str+=to_string(j);
                        str+="]";
                    }
                }
            }
        }
    }
    str+="}]";
    return str; //[10,5,#,5,{[#,4,4][>,4,1][>,5,5]}]
}


vector<string> parseurTerrain::split(const string str, const char delimiteur)
{
    vector<string> v;
    stringstream s(str);
    string sousElement;
    while (std::getline(s, sousElement, delimiteur))
    {
        if(sousElement.size() >0)
        {
            v.push_back(sousElement);
        }
    }
    return v;
}

vector<size_t> parseurTerrain::getPositionIndexesOfStrInChaine(string& chaine, string& str)
{
    //R�cup�re toutes les positions de delimit2 dans la chaine proprietes
    vector<size_t> positionsProprietes;
    size_t position = chaine.find(str, 0);
    while(position != string::npos)
    {
        positionsProprietes.push_back(position);
        position = chaine.find(str,position+1);
    }
    return positionsProprietes;
}

// Fonction � red�composer et g�n�raliser ult�rieurement
unique_ptr<terrain> parseurTerrain::parseFromString(string data,const string& nom)
{
    // D�composition de la chaine pour reconstituer un objet correspondant
    char delimit1{'{'};
    char delimit2 = ',';
    char delimit3 = '[';

    vector<string> decoupage1 = parseurTerrain::split(data,delimit1);
    string proprietes = decoupage1[0].substr(1,decoupage1[0].size()-1); // 10,10,#,5,
    string objetsGrille = decoupage1[1].substr(0,decoupage1[1].size()-2); // [mur,#,3,3][laser,>,3,0][cible,>,4,4]

    // Partie sp�cifique � l'objet terrain
    int nbLignes;
    int nbColonnes;
    int nbMiroirsMax;
    char caractereMurTerrain =' ';

    // conversions de string vers int
    vector<string> proprietesTerrain = parseurTerrain::split(proprietes,delimit2);
    nbLignes = stoi(proprietesTerrain[0]);
    nbColonnes = stoi(proprietesTerrain[1]);
    nbMiroirsMax =stoi(proprietesTerrain[3]);
    caractereMurTerrain = *(proprietesTerrain[2].c_str());
    mur m{caractereMurTerrain};
    auto terrainObtenu = make_unique<terrain>(nbLignes,nbColonnes,nbMiroirsMax,m,nom);
    //std::unique_ptr<terrain> terrainObtenu(new terrain(nbLignes,nbColonnes,nbMiroirsMax,m,nom));
    vector<string> objets = parseurTerrain::split(objetsGrille,delimit3);
    for(string obj : objets)
    {
        obj = obj.substr(0,obj.size()-1);
        // r�cup�rer les propri�t�s respectives
        vector<string> proprietesObj = parseurTerrain::split(obj,delimit2);
        if(proprietesObj[0] == "mur")
        {
            char car = proprietesObj[1].c_str()[0];
            int x=0;
            x =stoi(proprietesObj[2]);
            int y = 0;
            y = stoi(proprietesObj[3]);
            terrainObtenu->insereObjet(make_unique<mur>(car),x,y);
        }
        if(proprietesObj[0] == "cible")
        {
            char car = proprietesObj[1].c_str()[0];
            int x=0;
            x =stoi(proprietesObj[2]);
            int y = 0;
            y = stoi(proprietesObj[3]);
            terrainObtenu->insereObjet(make_unique<cible>(car),x,y);
        }
        if(proprietesObj[0] == "miroir")
        {
            char car = proprietesObj[1].c_str()[0];
            int x=0;
            x =stoi(proprietesObj[2]);
            int y = 0;
            y = stoi(proprietesObj[3]);
            terrainObtenu->insereObjet(std::make_unique<miroir>(car,1),x,y);
        }
        if(proprietesObj[0] == "laser")
        {
            char car = proprietesObj[1].c_str()[0];
            int x=0;
            x =stoi(proprietesObj[2]);
            int y = 0;
            y = stoi(proprietesObj[3]);
            terrainObtenu->insereObjet(make_unique<laser>(car),x,y);
        }
        if(proprietesObj[0] != "laser" && proprietesObj[0] != "cible" && proprietesObj[0]!= "mur" && proprietesObj[0]!= "miroir")
        {
            throw "Un objet du terrain n'a pas �t� identifi�";
        }
    }
    return move(terrainObtenu);
}


