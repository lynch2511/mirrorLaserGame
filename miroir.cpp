#include "miroir.h"

miroir::miroir()
{

}

miroir::miroir(char& caractere, int orientation) : d_caractereM{caractere}, d_orientation{orientation}
{

}

std::string miroir::recupererType() const
{
    std::string type =  typeid(*this).name();
    type = type.substr(1,type.length()-1);
    return type;
}

char miroir::recupererCaractere() const
{
    return d_caractereM;
}

void miroir::afficherObjet() const
{
    cout<<d_caractereM;
}

