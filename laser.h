#ifndef LASER_H
#define LASER_H
#include "objet.h"

#include <iostream>

class laser : public objet{
public:

    laser();
    laser(char& caractere);
    virtual char recupererCaractere() const override;
    virtual void afficherObjet() const override;
    virtual std::string recupererType() const override;
    virtual ~laser()=default;

private:
    char d_caractereLaser;
};

#endif //LASER_H
