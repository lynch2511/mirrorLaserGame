#include <iostream>
#include <vector>
#include "terrain.h"
#include "afficheur.h"
#include "terminal.h"
#include "mur.h"
#include "laser.h"
#include "jeu.h"
using namespace std;



int main()
{
    unique_ptr<afficheur> t(new terminal());
	jeu laserGame{t};
    try
    {
        laserGame.run();
    }
    catch(const char* msg)
    {
        cerr << msg << endl;
    }
    return 0;
}
