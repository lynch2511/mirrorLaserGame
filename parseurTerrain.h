#ifndef PARSEURTERRAIN_H
#define PARSEURTERRAIN_H

#include <iostream>
#include "terrain.h"
#include<vector>

class parseurTerrain{
public:
    static string parse(const terrain& t);
    static unique_ptr<terrain> parseFromString(string data,const string& nom); // on transfert la responsabilité de l'obejt a un autre pointeur avec unique_ptr
    static vector<size_t> getPositionIndexesOfStrInChaine(string& chaine, string& str);
    static vector<string> split(const string str, const char delimiteur);
};

#endif //PARSEURTERRAIN_H
