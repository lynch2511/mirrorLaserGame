#include "tool.h"

#include <sstream>
unsigned int tool::tryParse(string str)
{
    istringstream streamStr(str);
    unsigned int test = -1;
    if((streamStr>> test).fail())
    {
        return -1;
    }
    else
    {
        return test;
    }
}
