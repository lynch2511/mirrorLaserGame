#ifndef PARTIE_H
#define PARTIE_H

#include"terrain.h"
#include "afficheur.h"

class partie
{
    public:
        partie();
        partie(unique_ptr<terrain> t, unique_ptr<afficheur> a);
        void poserMiroir();
        void DebutPartie();
        void retirerMiroir();
        void tirer();
        void reinitialiserTerrain();
        void abandonnerPartie();
        void sauvegarderPartie();
        bool partieExiste(const string& nomFihcier);
        unique_ptr<terrain> recupererTerrain();
    private:
        void afficherTerrain();
        unsigned int MenuPartie();
        unique_ptr<terrain> d_terrain;
        unique_ptr<afficheur> d_afficheur;
};

#endif // PARTIE_H
