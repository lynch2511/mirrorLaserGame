#ifndef JEU_H
#define JEU_H

#include"terrain.h"
#include"partie.h"
#include <vector>
#include "tool.h"
#include "afficheur.h"

const string repertoireSauvegarde = "Sauvegardes/";
const string repertoireTerrains = "Terrains/";

class jeu
{
    public:
        jeu();
        jeu(unique_ptr<afficheur>& aff);
        unique_ptr<partie> recupererPartie();
        bool chargerTerrain(const std::string ficTerrain);
        std::string lireFichier(const std::string& fic) const;
        void run();

    private:
        bool chaineCaracTerrainConforme(std::string &chaine);
        unsigned int MenuPrincipal();
        unsigned int MenuNouvellePartie(vector<string>& terrains) const;
        void nouvellePartie();
        void chargerPartie();
        void chargerPartie(terrain &t);
        vector<string> chargerTerrains() const;
        vector<string> chargerParties() const;
        unique_ptr<partie> d_partie;
        unique_ptr<afficheur> d_afficheur;
};

#endif // JEU_H
