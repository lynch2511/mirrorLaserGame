#include "partie.h"
#include "jeu.h"
#include "mur.h"
#include <cstdlib>
#include "parseurTerrain.h"
#include <string>
#include "dirent.h"
#include "afficheur.h"
#include "terminal.h"

jeu::jeu()
{
}

jeu::jeu(unique_ptr<afficheur>& aff) : d_afficheur{move(aff)}, d_partie{}
{}

void jeu::run()
{
    unsigned int selection = MenuPrincipal();
    while (selection != 0)
    {
        switch(selection)
        {
          case 1: nouvellePartie();
                  break;
          case 2: chargerPartie();
                break;
        }
        selection = MenuPrincipal();
    }
    exit(0);
}

unsigned int jeu::MenuPrincipal()
{
    d_afficheur->reinitialiser();
    string titre;
    titre+="***************************\n";
    titre+="*      LASERGAME          *\n";
    titre+="***************************\n";
    titre+="MENU PRINCIPAL \n";

    string instructions = "Veuillez effectuer un choix : \n";
    vector<string> propositions{};
    propositions.push_back("Nouvelle partie");
    propositions.push_back("Charger une partie");

    // il reste encore un remaniement � faire ici: cr�er un objet menu  caract�ris� par les param�tres ci-dessous
    return d_afficheur->afficherMenu(titre,instructions,propositions);

}

// remaniement : Sortir l'affichage de la classe
unsigned int jeu::MenuNouvellePartie(vector<string>& terrains) const
{
    d_afficheur->reinitialiser();
    string titre=">> Nouvelle partie \n";
    string instructions = "Choisissez un terrain (les points d'exclamation indiquent un niveau de difficulte) : \n";

    // il reste encore un remaniement � faire ici: cr�er un objet menu  caract�ris� par les param�tres ci-dessous
    return d_afficheur->afficherMenu(titre,instructions,terrains);
}

vector<string> jeu::chargerTerrains() const
{
    DIR *rep;
    vector<string> s;
    rep=opendir(repertoireTerrains.c_str());
    if(rep==nullptr) { cout<<"Impossible d'ouvrir le r�pertoire "+ repertoireTerrains +" dans le r�pertoire courant."<<endl; return s;}
    dirent *elem;
    while((elem=readdir(rep))!= nullptr)
    {
        if(string(elem->d_name) !=".." && string(elem->d_name) !="." && elem->d_type!=DT_DIR)
        {
            s.push_back(elem->d_name);
        }
    }
    closedir(rep);
    return s;
}

vector<string> jeu::chargerParties() const
{

}

unique_ptr<partie> jeu::recupererPartie()
{
    return move(d_partie);
}

bool jeu::chaineCaracTerrainConforme(string &chaine)
{
    if(chaine != "")
    {
        return true; // TODO
    }
    return false;
}

void jeu::nouvellePartie()
{
    vector<string> terrains = chargerTerrains();
    int selectionTerrain = MenuNouvellePartie(terrains);
    while(selectionTerrain != 0)
    {
        if(chargerTerrain(repertoireTerrains+terrains[selectionTerrain-1]))
        {
            d_partie->DebutPartie();
        }
        selectionTerrain = MenuNouvellePartie(terrains);
    }
}

void jeu::chargerPartie()
{
    //TO DO
}

// Cette m�thode pourra �tre s�quenc�e en plusieurs sous m�thodes plus tard
bool jeu::chargerTerrain(const string ficTerrain)
{
    bool estCharge = false;
    // Chargement des donn�es du fichier*/
    ifstream fichier(ficTerrain);
    string resultat;
    if(fichier)
    {
        getline(fichier,resultat); // r�cup�rer la premiere ligne du fichier uniquement
        if(chaineCaracTerrainConforme(resultat))
        {
            //Construction d'un objet terrain � partir de la chaine de caract�res
            unique_ptr<terrain> terrainObtenu{move(parseurTerrain::parseFromString(resultat,ficTerrain))};
            auto p = make_unique<partie>(move(terrainObtenu),move(d_afficheur));
            d_partie = move(p);
            estCharge = true;
        }
        else
        {
            cout<<"La chaine de caractere contenue dans le fichier ne peut être convertie en objet terrain.";
        }
    }
    else
    {
        cout<<"Impossible d'ouvrir le fichier specifie";
    }
    fichier.close();
    return estCharge;

}

std::string jeu::lireFichier(const std::string& fic) const
{
    ifstream fichier(fic);
    std::string resultat="";
    if(fichier)
    {
        getline(fichier,resultat); // r�cup�rer la premiere ligne du fichier uniquement
    }
    else
    {
        cout<<"Impossible d'ouvrir le fichier specifie";
    }
    return resultat;
}


