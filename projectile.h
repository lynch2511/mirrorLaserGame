#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "objet.h"
#include "sensDeplacement.h"

class projectile : public objet{
public:
    projectile();
    projectile(char& caractere);
    virtual char recupererCaractere() const override;
    virtual void afficherObjet() const override;
private:
    char d_caractereM;




};

#endif

