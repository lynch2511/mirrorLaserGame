#ifndef CIBLE_H
#define CIBLE_H

#include "objet.h"

class cible : public objet{
public:
    cible(char& caractere);
    cible();
    virtual char recupererCaractere() const override;
    virtual std::string recupererType() const override;
    virtual void afficherObjet() const override;
private:
    char d_caractere;
};

#endif //CIBLE_H
