#include "../doctest.h"
#include "../jeu.h"
#include "../parseurTerrain.h"
#include "../terminal.h"

const string repTest= "fichiersTests/";

TEST_CASE("Test de Jeu")
{
    SUBCASE("Test du chargement des terrains")
    {
        unique_ptr<afficheur> a(new terminal());
        jeu jeuTest{a};
        string ficTerrainVide{repTest+"fichierTestVide.txt"};
        string ficTerrain{repTest+"fichierTest.txt"};
        SUBCASE("Le chargement d'un fichier vide échoue")
        {
            bool resultatObtenu = jeuTest.chargerTerrain(ficTerrainVide);
            bool resultatAttendu = false;
            REQUIRE_EQ(resultatObtenu,resultatAttendu);
        }

        SUBCASE("Le chargement d'un fichier contenant un terrain est correct")
        {
            bool resultatObtenu = jeuTest.chargerTerrain(ficTerrain);
            bool resultatAttendu = true;
            REQUIRE_EQ(resultatObtenu,resultatAttendu);
        }

        SUBCASE("Le terrain est correctement chargé par le jeu dans la partie")
        {
            jeuTest.chargerTerrain(ficTerrain);
            unique_ptr<partie> partieObtenue{std::move(jeuTest.recupererPartie())};
            unique_ptr<terrain> terrainObtenu{std::move(partieObtenue->recupererTerrain())};

            string data = jeuTest.lireFichier(ficTerrain);
            string terrainObtenuToString = parseurTerrain::parse(*terrainObtenu);

            REQUIRE_EQ(data,terrainObtenuToString);

        }



    }

}

