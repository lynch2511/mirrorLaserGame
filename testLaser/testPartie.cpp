#include <fstream>
#include "../doctest.h"
#include "../partie.h"
#include "../cible.h"
#include "../laser.h"
#include "../terrain.h"
#include "../mur.h"
#include "../miroir.h"
#include "../parseurTerrain.h"
#include "../objet.h"
#include "../afficheur.h"
#include "../terminal.h"
#include "../jeu.h"


TEST_CASE("La pose d'un miroir est correct")
{
    char test = '#';
    mur murTest{test};
    char charMiroir = '/';

    unique_ptr<terrain> terrainTest(new terrain(10,10,5,murTest,"test"));

    terrainTest->insereObjet(make_unique<mur>(test),3,6);

    SUBCASE("La pose d'un miroir dans une case vide est correct") {
        char res = ' ';
        terrainTest->insereObjet(make_unique<miroir>(charMiroir,90),4,6);
        auto testobj = terrainTest->getObjet(4,6);
        res = (*testobj).recupererCaractere();
        REQUIRE_EQ(res,charMiroir);

    }

    SUBCASE("La pose d'un miroir dans une case utilisée est impossible") {
        auto testobj = terrainTest->getObjet(3,6);
        bool estMiroir = false;

        terrainTest->insereObjet(make_unique<miroir>(charMiroir,90),3,6);
        testobj = terrainTest->getObjet(3,6);

        char res = (*testobj).recupererCaractere();

        if(res == charMiroir) {
            estMiroir = true;
        }

        REQUIRE_EQ(estMiroir,false);
        }
}


TEST_CASE("le retrait d'un miroir est correct")
{
    char test = '#';
    mur murTest{test};
    char charMiroir = '/';
    bool estVide = false;

    unique_ptr<terrain> terrainTest(new terrain(10,10,5,murTest,"test"));
    terrainTest->insereObjet(make_unique<mur>(test),3,6);
    terrainTest->insereObjet(make_unique<miroir>(charMiroir,90),4,6);

    SUBCASE("Le retrait d'un miroir est correct") {
        terrainTest->retirerObjet(4,6);

        auto testobj = terrainTest->getObjet(4,6);

        if(testobj == nullptr)
        {
            estVide = true;
        }

        REQUIRE_EQ(estVide,true);
    }

    SUBCASE("Le retrait d'un objet autre que miroir est impossible") {

        SUBCASE("Le retrait d'une bordure est impossible") {
            terrainTest->retirerObjet(0,0);
            auto testobj = terrainTest->getObjet(0,0);

            if(testobj == nullptr)
            {
                estVide = true;
            }

            REQUIRE_EQ(estVide,false);
        }

        SUBCASE("Le retrait d'un mur est impossible") {
            terrainTest->retirerObjet(3,6);
            auto testobj = terrainTest->getObjet(3,6);

            if(testobj == nullptr)
            {
                estVide = true;
            }

            REQUIRE_EQ(estVide,false);
        }
    }

}


TEST_CASE("Le deploiement du tir est correct")
{

}


TEST_CASE("La réinitialisation d'un terrain est correct")
{

}


TEST_CASE("L'abandon de la partie est correct")
{

}


TEST_CASE("La sauvegarde d'une partie est correcte")
{
    //Ce subcase ne passe pas car il faut modifier la methode partieExiste pour qu'elle puisse etre lancé depuis le repertoire parent
    SUBCASE("verification de l'existance fichier fonctionne correctement")
    {
        partie p{};
        string nomFichier="testPartie.txt";
        ofstream fichierOut( "../"+repertoireSauvegarde+"/"+ nomFichier);
        bool test=true;
        if(!p.partieExiste(nomFichier))
        {
            test=false;
        }
        //REQUIRE_EQ(test,true);
    }

    SUBCASE("sauvegarde partie s'effectue")
    {

    }
}
