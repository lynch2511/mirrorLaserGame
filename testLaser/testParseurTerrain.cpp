#include "../doctest.h"
#include <sstream>
#include "../parseurTerrain.h"
#include "../laser.h"
#include "../cible.h"
#include <vector>
#include <iostream>
using namespace std;

/*
Exemple de formatage :
------------------------------------
contenu de fichier.txt :
[10,10,#,5,{[mur,#,1,4][mur,#,2,4][mur,#,3,4][laser,>,4,0]}]
------------------------------------

Dessin correspondant :
X X X X X X X X X X
X . . . X . . . . X
X . . . X . . . . X
X . . . X . . . . X
> . . . . . . . . X
X . . . . . . . . X
X . . . . . . . . X
X . . . . . . . . X
X . . . . . . . . X
X X X X X X X X X X

*/
TEST_CASE("La chaine de caract�re donn�e est correctement format�e")
{

}

TEST_CASE("La conversion d'une chaine de caract�re correctement format�e en objet terrain est correcte")
{
    std::string data = "[10,10,#,5,{[mur,#,3,3][laser,>,3,0][cible,<,4,4]}]";
    // cr�ation de l'objet terrain attendu
    char test = '#';
	char laserTest ='>';
	char cibleTest = '<';
	mur murTest{test};
	terrain terrainAttendu{10,10,5,murTest,"NomTerrain"};
	terrainAttendu.insereObjet(make_unique<mur>(test),3,3);
	terrainAttendu.insereObjet(make_unique<laser>(laserTest),3,0);
	terrainAttendu.insereObjet(make_unique<laser>(cibleTest),4,4);
    //terrain* pointeur;
	unique_ptr<terrain> terrainObtenu{std::move(parseurTerrain::parseFromString(data,"NomTerrain"))};
	//pointeur = parseurTerrain::parseFromString(data).release(); // alternative

    bool resultat = false;
    if(terrainAttendu == *terrainObtenu) {resultat = true;}
    REQUIRE_EQ(true,resultat);
}
