#include <fstream>
#include "../doctest.h"
#include "../cible.h"
#include "../laser.h"
#include "../terrain.h"
#include "../mur.h"
#include "../parseurTerrain.h"

using namespace std;
TEST_CASE("lecture ecriture du fichier correct")
{
    std::ifstream fichierIN("../fichierTest.txt");
    bool resultat{false};
    bool attendu{true};
    if(fichierIN)
    {
        resultat=true;
    }
    REQUIRE_EQ(resultat,attendu);
}

TEST_CASE("L'écriture d'un terrain dans un fichier est correcte")
{
    std::ofstream file;
    // Effacer le contenu du fichier test
    file.open("../fichierTest2.txt", ios::out | ios::trunc);
    file.close();

    //file.open("../fichierTest.txt",ios::app);
    char test = '#';
    char test2 ='#';
    char testLaser = '>';
    laser laserTest{testLaser};
    mur murTest{test};
    terrain testTerrain{10,10,5,murTest,"NomTerrain"};
    testTerrain.insereObjet(make_unique<mur>(test2),1,3);
    testTerrain.insereObjet(make_unique<mur>(test2),1,1);
    testTerrain.insereObjet(make_unique<laser>(testLaser),4,0);

    std::ofstream fichier("../fichierTest2.txt");
    std::string resultat=" ";
    if(fichier)
    {
        resultat = parseurTerrain::parse(testTerrain);
        fichier<<resultat<<endl;
    }
    else
    {
        cout<<"Impossible d'ouvrir le fichier spécifié";
    }

    std::string resultatObjectif="[10,10,#,5,{[mur,#,1,1][mur,#,1,3][laser,>,4,0]}]";
    REQUIRE_EQ(resultat,resultatObjectif);
}

TEST_CASE("L'ajout d'un objet dans la grille est correct")
{
    char test = '#';
    mur murTest{test};
    terrain testTerrain{10,10,5,murTest,"NomTerrain"};
    testTerrain.insereObjet(make_unique<mur>(test),4,5);
    auto testobj = testTerrain.getObjet(4,5);
    char res = ' ';
    if(testobj!=nullptr)
    {
        res = (*testobj).recupererCaractere();
    }
    REQUIRE_EQ(res,test);
}

TEST_CASE("Le retrait d'un objet dans la grille est correct")
{

}

TEST_CASE("Le terrain est correctement chargé dans le jeu")
{
    std::ifstream fichier("../fichierTest2.txt");
    std::string resultat=" ";
    if(fichier)
    {
        getline(fichier,resultat); // récupérer la premiere ligne du fichier uniquement
    }
    else
    {
        cout<<"Impossible d'ouvrir le fichier spécifié";
    }

    // supposons la fonction de parseur inverse correcte
    string resultatAttendu = resultat;
    unique_ptr<terrain> t {std::move(parseurTerrain::parseFromString(resultatAttendu,"nom"))};
    string resultatObtenu = parseurTerrain::parse(*t);
    REQUIRE_EQ(resultatObtenu,resultat);

}

