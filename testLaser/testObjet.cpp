#include "../doctest.h"
#include "../objet.h"
#include "../mur.h"
#include "../laser.h"
#include <iostream>
#include <string>


TEST_CASE("Test des sous types Objet")
{
    SUBCASE("Un objet d�riv� renvoie bien son type")
    {
        char c= '#';
        laser m{c};
        std::string resObtenu = m.recupererType();
        std::string resAttendu = "laser";
        REQUIRE_EQ(resObtenu,resAttendu);
    }
}
