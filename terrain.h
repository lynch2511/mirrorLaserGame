#ifndef TERRAIN_H
#define TERRAIN_H

#include "objet.h"
#include "mur.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include "cible.h"
#include <fstream>
#include <memory>
#include "laser.h"
#include "sensDeplacement.h"
#include "position.h"

using namespace std;
// A CHANGER RAPIDEMENT : le terrain charg� doit donner les caracteres des types !! rien en dur dans le programme
enum { MUR = '#', LASER ='>', CIBLE = '<', MIROIR = '/' }; // Manque \ car en C++ c'est \\ (pas un char...)

//const sensDeplacement HAUT {0,1}; const sensDeplacement DROITE {1,0};
//const sensDeplacement BAS {0,-1}; const sensDeplacement GAUCHE {-1,0};

class terrain
{
public:
    terrain();
	terrain(const int nbLignes, const int nbColonnes, const int nbMiroirsMax, const mur &m,const string& nom);
	int nbLignes() const;
	int nbColonnes() const;
	int nbMiroirsMax() const;
	int nbMiroirsPoses() const;
	int nbMiroirsDisponibles() const;
	string Nom() const;
	/*position getPosition() const;*/
	mur getMur() const;
    objet* getObjet(int x, int y) const;
	void show() const;
	void insereObjet(unique_ptr<objet> obj, const int x, const int y);
	void retirerObjet(const int x,const int y);
	bool operator==(const terrain& autreTerrain) const;
	bool tirer();
	virtual ~terrain();

private:
	void initialiserTerrainVide(const int nbLignes, const int nbColonnes);
	vector<vector<unique_ptr<objet>>> d_grille;
	mur d_mur;
    laser d_laser;
    string d_nom;
	int d_nbLignes, d_nbColonnes, d_nbMiroirsMax;
	/*position d_pos; //position du point de d�part*/

};

#endif // TERRAIN_H
