#ifndef MUR_H
#define MUR_H

#include "objet.h"
#include <iostream>


class mur : public objet{
public:
    mur();
    mur(char caractere);
    virtual char recupererCaractere() const override;
    virtual std::string recupererType() const override;
    virtual void afficherObjet() const override;
    virtual ~mur() = default;

private:
    char d_caractereM;

};

#endif //MUR_H
