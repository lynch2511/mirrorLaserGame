#ifndef AFFICHEUR_H
#define AFFICHEUR_H

#include"terrain.h"

class afficheur
{
    public:
        virtual ~afficheur();
        virtual void afficherGrille(terrain& t) const =0;
        virtual void reinitialiser() const = 0;
        virtual int afficherMenu(string titre, string instructions, vector<string> propositions) const =0;
    private:

};

#endif
